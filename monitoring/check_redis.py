#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import datetime

LOG_DIR = "/var/log/Omantel/monitoring.log"
CHECK_NAME = "Redis check"
USER = "streamsadmin"
STATUS = 0

from rediscluster import StrictRedisCluster
startup_nodes = [{"host": "10.164.61.18", "port": "7000"}]
try:
        r = StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=True)

        r.set("test", "test")
        r.delete("test")
except:
        print "[ERROR] redis cluster test FAIL"
        print "please contact AF developers Team"
	STATUS = 1

print CHECK_NAME, "OK"

f = open(LOG_DIR,"a")
f.write("%s,%s,%s,%d\n" % (datetime.datetime.now().isoformat(),CHECK_NAME, USER, STATUS))
f.close()

