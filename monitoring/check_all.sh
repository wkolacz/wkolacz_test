#!/bin/bash
cd `dirname $0`
./check_streams.sh
./check_lbs_1.sh
./check_arbor.py
#./check_hbase.sh
./check_redis.py
./check_rtcs.py
./check_scare.sh
./check_scg_edr.py
./check_hss.sh
#./check_sdl.sh
./check_csr.sh
./free_space.sh
./free_memory.sh
