#!/bin/bash

command=$( df -m | grep -iv use | grep -iv cdr_store | awk '{print $6 "-"$5}' )
warrning_usage=85%
max_usage=92%

for tmp in `echo $command`
do
#	echo $tmp
	tmp_path=`echo $tmp | awk -F- '{print $1}'`
	tmp_usage=`echo $tmp | awk -F- '{print $2}'`
#	echo $tmp_path
#	echo $tmp_usage
	if [ ${tmp_usage%?} -ge ${max_usage%?} ]; then
		echo "#[ERROR] Max space usage exceeded. Partition $tmp_path in usage is at $tmp_usage."
	elif [ ${tmp_usage%?} -ge ${warrning_usage%?} ] && [ ${tmp_usage%?} -lt ${max_usage%?} ]; then
		echo "#[WARNING]. Partition $tmp_path in usage is at $tmp_usage."
	else echo "#[OK] Partition $tmp_path usage at $tmp_usage."
	fi
done
