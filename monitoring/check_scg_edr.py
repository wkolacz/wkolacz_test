#!/usr/bin/env python
#-*- coding: utf-8 -*-

import datetime
import subprocess
import os

command = 'uname -n'
p = subprocess.Popen(command, universal_newlines=False, shell=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
text = p.stdout.read().strip()

if text in 'wat-af-strms01p-v.omantel.om':
	NSUFFIX = ""
else:
	NSUFFIX = "2"


LOG_DIR = "/var/log/Omantel/monitoring.log"
MIN_FILES = 9000
SCAN_DIR = "/af_data/scg/edr_protocol%s/archive" % (NSUFFIX,)
SCAN_DIR_2 = "/af_data/scg/edr_protocol%s/processing" % (NSUFFIX,)
SCAN_TIME = "-72hours"
CHECK_NAME = "SCG EDR Protocol check"
USER = os.getlogin()
MAX_FILES = 1000

command = 'find -L ' + SCAN_DIR + ' -ignore_readdir_race -maxdepth 1 -type f -newermt \"' + SCAN_TIME + '\" 2>&1 | grep -iv "No such"'
#print command
p = subprocess.Popen(command, universal_newlines=False, shell=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
text = p.stdout.read()
retcode = p.wait()

command = 'find -L ' + SCAN_DIR_2 + ' -ignore_readdir_race -maxdepth 1 -type f'
#print command
p = subprocess.Popen(command, universal_newlines=False, shell=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
text2 = p.stdout.read()
retcode2 = p.wait()

STATUS = 0
files2 = len(text2.split('\n'))
files = len(text.split('\n'))

if retcode != 0 or retcode2 !=0:
	print CHECK_NAME, "#[ERROR] Command exited with code ", retcode, ", Please contact AF developers Team \n\nVariable text:",text," \nVariable retcode:", retcode, " \nVariable text2:", text2, " \nVariable retcode2:", retcode2, " \nVariable files2:", files2, " \nVariable files:", files
	STATUS = 1
elif files < MIN_FILES:
	print CHECK_NAME, "#[ERROR] Found ", files, " files, excepted less ", MIN_FILES, ". Please contact AF developers Team"
	STATUS = 2
elif files2 > MAX_FILES:
	print CHECK_NAME, "#[ERROR] Found ", files, " files, excepted max ", MAX_FILES, ". Please contact AF developers Team"
	STATUS = 4
else:
	print CHECK_NAME, "OK"

f = open(LOG_DIR,"a")
f.write("%s,%s,%s,%d\n" % (datetime.datetime.now().isoformat(),CHECK_NAME, USER, STATUS))
f.close()

