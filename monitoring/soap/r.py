#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import sys
import datetime

url = sys.argv[1] #./r.py http://10.64.172.9:7943/AF/ADP/LBSService test_lbs.txt

f = open(sys.argv[2])
xml = f.read(10000000)
f.close()

headers = {'Content-Type': 'application/xml'}

print requests.post(url, data=xml, headers=headers, timeout=10.0).text

