#!/usr/bin/env python
#-*- coding: utf-8 -*-

import datetime
import subprocess
import os

LOG_DIR = "/var/log/Omantel/monitoring.log"
MIN_FILES = 100
SCAN_DIR = "/mnt/arbor"
SCAN_TIME = "-72hours"
CHECK_NAME = "Arbor check"
USER = os.getlogin()

command = 'find -L ' + SCAN_DIR + ' -maxdepth 1 -type f -newermt \"' + SCAN_TIME + '\"'
p = subprocess.Popen(command, universal_newlines=False, shell=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
text = p.stdout.read()
retcode = p.wait()

STATUS = 0

files = len(text.split('\n'))
#print "there is", files, "files"
if retcode != 0:
	print CHECK_NAME, "[ERROR] Command exited with code ", retcode, ", Please contact AF developers Team"
	STATUS = 1
elif files < MIN_FILES:
	print CHECK_NAME, "[ERROR] Found ", files, " files, not ", MIN_FILES, ". Please contact AF developers Team"
	STATUS = 2
else:
	print CHECK_NAME, "OK"

f = open(LOG_DIR,"a")
f.write("%s,%s,%s,%d\n" % (datetime.datetime.now().isoformat(),CHECK_NAME, USER, STATUS))
f.close()
