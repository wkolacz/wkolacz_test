#/bin/bash
source /home/streamsadmin/.bash_profile

deploy_with_log() {
        streamtool submitjob -i AF01 $1 --jobname `basename $1` < /etc/Omantel/credentials
        echo $1 " : " `date` >> /af_data/logs/deployed.log
}

ERR=0

result=`/opt/ibm/InfoSphere_Streams/4.1.0.0/java/jre/bin/java -jar /home/streamsadmin/monitoring/mq-checker.jar -h 10.64.162.21 -m OMLN101P -c AFADP.SVRCONN -q AF.CHL.COMMUNICATION.SERVICE.RESP -p 1436 | grep "current depth is" | awk -F'[' '{print $3}' | awk -F ']' '{print $1}'`

resulthss2=`/opt/ibm/InfoSphere_Streams/4.1.0.0/java/jre/bin/java -jar /home/streamsadmin/monitoring/mq-checker.jar -h 10.64.162.22 -m OMLN101P -c AFADP.SVRCONN -q AF.CHL.COMMUNICATION.SERVICE.RESP -p 1436 | grep "current depth is" | awk -F'[' '{print $3}' | awk -F ']' '{print $1}'`


APP_GLOB="CommunicationServiceResponse"
#SetDeviceLocation0
APPNAME="CommunicationServiceResponse0"
APPPATTERN="application::CommunicationServiceResponse0_"
APP="/home/streamsadmin/AF_modules/core/application.CommunicationServiceResponse0.sab"
RESTART_NODE1="restarted - no"
if [ $result -eq $result ]; then
	if [ $result -le 1000 ]; then
		echo "$APPNAME [$result messages] OK"
	elif [ $result -gt 1000 ];
	then
		echo "There is [$result] messages. Restarting $APPNAME job. Contact with Level3 Support is not required."
		RESTART_ID=`streamtool lsjob -i AF01 </etc/Omantel/credentials | grep "$APPPATTERN" | awk '{print $1}'` 
		CANCELJOB=`streamtool canceljob -i AF01 $RESTART_ID </etc/Omantel/credentials >/dev/null 2>&1`
		streamtool submitjob -i AF01 $APP --jobname `basename $APP` < /etc/Omantel/credentials
        echo $APP " : " `date` >> /af_data/logs/deployed.log		
		RESTART_NODE1="restarted - yes"
        ERR=1
	else
		echo "[ERROR] $APPNAME other error. Please contact Level3 Support."
		ERR=4
	fi
else
	echo "[ERROR] $APPNAME error, unexpected out of script. Please contact Level3 Support"
	ERR=8
fi

#SetDeviceLocation1
APPNAME="CommunicationServiceResponse"
APPPATTERN="application::CommunicationServiceResponse1_"
APP="/home/streamsadmin/AF_modules/core/application.CommunicationServiceResponse1.sab"
RESTART_NODE2="restarted - no"
if [ $resulthss2 -eq $resulthss2 ]; then
	if [ $resulthss2 -le 1000 ]; then
		echo "$APPNAME [$resulthss2 messages] OK"
	elif [ $resulthss2 -gt 1000 ];
	then
		echo "There is [$resulthss2] messages. Restarting $APPNAME job. Contact with Level3 Support is not required."
		RESTART_ID=`streamtool lsjob -i AF01 </etc/Omantel/credentials | grep "$APPPATTERN" | awk '{print $1}'` 
		CANCELJOB=`streamtool canceljob -i AF01 $RESTART_ID </etc/Omantel/credentials >/dev/null 2>&1`
		streamtool submitjob -i AF01 $APP --jobname `basename $APP` < /etc/Omantel/credentials
        echo $APP " : " `date` >> /af_data/logs/deployed.log		
		RESTART_NODE2="restarted - yes"
        ERR=$((ERR+16))
	else
		echo "[ERROR] $APPNAME other error. Please contact Level3 Support."
		ERR=$((ERR+32))
	fi
else
	echo "[ERROR] $APPNAME error, unexpected out of script. Please contact Level3 Support"
	ERR=$((ERR+64))
fi

echo "`date -u +"%Y-%m-%dT%H:%M:%S.000000"`,CSR check,`whoami`,$ERR,$result,ADP0($RESTART_NODE1),$resulthss2,ADP1($RESTART_NODE2)" >> /var/log/Omantel/monitoring.log
