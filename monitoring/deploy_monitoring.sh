#!/bin/bash

echo '*** DEPLOY MONITORING ***' &&
ssh prod11b 'echo "" > .nsuffix' &&
ssh prod11z 'echo "" > .nsuffix' &&
ssh prod11  'echo "" > monitoring/.nsuffix' &&
ssh prod12b 'echo "2" > .nsuffix' &&
ssh prod12z 'echo "2" > .nsuffix' &&
ssh prod12  'echo "2" > monitoring/.nsuffix' &&
scp -r ./scripts/monitoring/* prod11b:/home/s.al.balushi/ 1>&2 &&
scp -r ./scripts/monitoring/* prod11z:/home/l.al.zaabi/ 1>&2 &&
scp -r ./scripts/monitoring prod11:/home/streamsadmin/ 1>&2 &&
scp -r ./scripts/monitoring/* prod12b:/home/s.al.balushi/ 1>&2 &&
scp -r ./scripts/monitoring/* prod12z:/home/l.al.zaabi/ 1>&2 &&
scp -r ./scripts/monitoring prod12:/home/streamsadmin/ 1>&2 &&
echo '*** DONE ***'

