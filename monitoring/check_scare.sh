#!/bin/bash

CHECK_NAME="Scare check"
SCANNED_ERR=""
SCAN_DIRS="
/mnt/scare/ps/xdr/user/other
/mnt/scare/ps/xdr/user/web
/mnt/scare/ps/xdr/user/stream
/mnt/scare/cs/xdr/aiu/mm
/mnt/scare/cs/xdr/aiu/moc
/mnt/scare/cs/xdr/aiu/mosms 
/mnt/scare/cs/xdr/aiu/mtc
/mnt/scare/cs/xdr/aiu/mtsms
/mnt/scare/ps/xdr/gbiups/gbiups
/mnt/scare/ps/xdr/gn/gtpv0v1
/mnt/scare/ps/xdr/s1mme/s1handover
/mnt/scare/ps/xdr/s6a/s6a
/mnt/scare/ps/xdr/user/stream
/mnt/scare/ps/xdr/gi/sgidiameter
/mnt/scare/ps/xdr/gtpv2/gtpv2
/mnt/scare/ps/xdr/s1mme/s1mme
/mnt/scare/ps/xdr/user/other
/mnt/scare/ps/xdr/user/web"

log_str="`date -u +"%Y-%m-%dT%H:%M:%S.000000"`,Scare check,`whoami`"


for entry in $SCAN_DIRS
do
        if [ `date +%M --date '-10 min'` -lt 30 ]; then
                result=`ls $entry | grep "dsi.\`date +%Y%m%d_%H --date '-10 min'\`[0-2][0-9]"| wc -l | grep [0-9]`
                exit_code=$?
        else
                result=`ls $entry | grep "dsi.\`date +%Y%m%d_%H --date '-10 min'\`[3-5][0-9]"| wc -l | grep [0-9]`
                exit_code=$?
        fi

        if [ $exit_code -ne 0 ]; then
                printf "[ERROR] $CHECK_NAME, Command exited with code $exit_code, Please contact AF developers Team\n"
                STATUS=1
                SCANNED_ERR=$entry
                log_str="$log_str\n,$result,$STATUS,$entry"
                continue
        elif [ $result -gt 0 ]; then
                printf "$CHECK_NAME, OK\n"
                STATUS=0
                log_str="$log_str\n,$result,$STATUS,$entry"
                continue
        fi

        result=`ls $entry | grep -E "dsi.\`date -d '1 hour ago' +%Y%m%d\`_\`date -d '1 hour ago' +%H\`|dsi.\`date -d '2 hour ago' +%Y%m%d\`_\`date -d '2 hour ago' +%H\`" | wc -l | grep [0-9]`
        exit_code=$?

        if [ $exit_code -ne 0 ]; then
                printf "[ERROR] $CHECK_NAME, Command exited with code $exit_code, Please contact AF developers Team\n"
                STATUS=1
                SCANNED_ERR=$entry
                log_str="$log_str\n,$result,$STATUS,$entry"
                continue
        elif [ $result -gt 0 ]; then
                printf "[WARNING] $CHECK_NAME, No files found in $entry, since 30 min. You dont have to contact with af developers\n"
                STATUS=0
                log_str="$log_str\n,$result,$STATUS,$entry"
        else
                printf "[ERROR] $CHECK_NAME, No files found in $entry, Please contact AF developers Team\n"
                STATUS=2
                SCANNED_ERR=$entry
                log_str="$log_str\n,$result,$STATUS,$entry"
        fi
done

echo -e $log_str >> /var/log/Omantel/monitoring.log
