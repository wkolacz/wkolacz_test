#!/usr/bin/env python
#-*- coding: utf-8 -*-

#import datetime
from datetime import datetime, timedelta
import subprocess
import os

d = datetime.now() - timedelta(minutes=10)
LOG_DIR = "/var/log/Omantel/monitoring.log"
MIN_FILES = 2 # +1 for new line
SCAN_DIR = "/mnt/rtcs/" + d.strftime("%Y%m%d") + '/'
CHECK_NAME = "Rtcs check"
USER = os.getlogin()

#command = 'find -L ' + SCAN_DIR + ' -maxdepth 2 -type f -newermt \"' + SCAN_TIME + '\"'
if int(d.strftime("%M")) < 30:
	command = 'ls ' + SCAN_DIR + ' | grep ' + d.strftime("%Y%m%d_%H") + '[0-2][0-9]'
else:
	command = 'ls ' + SCAN_DIR + ' | grep ' + d.strftime("%Y%m%d_%H") + '[3-5][0-9]'

p = subprocess.Popen(command, universal_newlines=False, shell=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
text = p.stdout.read()
retcode = p.wait()

STATUS = 0

files = len(text.split('\n'))
print "there is", files, "files and the command was: ", command
if retcode != 0:
	print CHECK_NAME, "[ERROR] Command exited with code ", retcode, ", Please contact AF developers Team"
	STATUS = 1
elif files < MIN_FILES:
	print CHECK_NAME, "[ERROR] Found ", files, " files, excepted less ", MIN_FILES, ". Please contact AF developers Team"
	STATUS = 2
else:
	print CHECK_NAME, "OK"

f = open(LOG_DIR,"a")
f.write("%s,%s,%s,%d\n" % (datetime.now().isoformat(),CHECK_NAME, USER, STATUS))
f.close()
