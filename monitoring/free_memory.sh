#!/bin/bash

command=$( cat /proc/meminfo | awk '{print $1$2}' )
warrning_usage=85
max_usage=90
MemTotal=0
MemAvailable=0

for tmp in `echo $command`
do
	if [ `echo $tmp | grep -i memtotal` ]; then
		MemTotal=`echo $tmp | grep -i memtotal | awk -F: '{print $2}'`
	elif [ `echo $tmp | grep -i memavailable` ]; then
		MemAvailable=`echo $tmp | grep -i memavailable | awk -F: '{print $2}'`
	fi
done
if [ `echo "100-($MemAvailable/($MemTotal/100))" |bc` -gt $max_usage ]; then
	echo "#[ERROR] - `echo "100-($MemAvailable/($MemTotal/100))" |bc`% memory usage. MemTotal - $MemTotal kB, MemAvailable - $MemAvailable kB"
elif [ `echo "100-($MemAvailable/($MemTotal/100))" |bc` -gt $warrning_usage ]; then
	echo "#[WARRNING] - `echo "100-($MemAvailable/($MemTotal/100))" |bc`% memory usage. MemTotal - $MemTotal kB, MemAvailable - $MemAvailable kB"
else echo "#[OK] - `echo "100-($MemAvailable/($MemTotal/100))" |bc`% memory usage. MemTotal - $MemTotal kB, MemAvailable - $MemAvailable kB"
fi
