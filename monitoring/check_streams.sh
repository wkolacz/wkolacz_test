#!/bin/bash

source /home/streamsadmin/.bash_profile

ERR="0"

streams_jobs="\
application.AF_Main_Module.sab \
application.ArborParser \
application.CellIDCustomerTracker.sab \
application.CellIDCustomerTracker_ADP0.sab \
application.CellIDCustomerTracker_ADP1.sab \
application.CustomerProfileReader.sab \
application.Filter_parser.sab \
application.GeolocationAF.sab \
application.HangoutsDetector.sab \
application.HSSResponse1.sab \
application.HSSResponse2.sab \
application.HSSResponseAggregator.sab \
application.IMEI_Check.sab \
application.LocationHangoutFilter.sab \
application.RtcsLogicProcess.sab \
application.RtcsParseProcess.sab \
application.RtcsUntarProcess.sab \
application.SCG_EDRProtocolParser2.sab \
application.SCG_EDRProtocolParser.sab \
\
SCARE_CS_MM \
SCARE_CS_MOC \
SCARE_CS_MOSMS \
SCARE_CS_MTC \
SCARE_CS_MTSMS \
SCARE_PS_GBIUPS \
SCARE_PS_GTP0V1 \
SCARE_PS_S1HANDOVER \
SCARE_PS_S1MME \
SCARE_USER_OTHER \
SCARE_USER_WEB \
\
application.Statistics_Sink.sab \
application.UsageAggregator.sab \
application.CommunicationServiceResponseXMS1.sab \
application.CommunicationServiceResponseXMS.sab \
application.Diameter_Interface.sab \

"

tmpfname=`mktemp`
streamtool lsjobs -d StreamsDomain -i AF01 </etc/Omantel/credentials > "$tmpfname"

for sj in $streams_jobs; do

	# authorize to streamtool and find if the job is running
	x=`cat "$tmpfname" |grep "$sj" | grep "Running" | grep "yes"`
	if [ "$x" == "" ] ; then
	        #if application is not working, run it
	        echo "[ERROR] $sj is not working"
	        ERR="1"
	fi
done


echo "`date -u +"%Y-%m-%dT%H:%M:%S.000000"`,AF_Streams check,`whoami`,$ERR" >> /var/log/Omantel/monitoring.log

if [ "$ERR" == "0" ] ; then
	echo "AF_Streams check OK"
else
	cat "$tmpfname" >> /var/log/Omantel/monitoring.log
fi


rm -f "$tmpfname"

